from trytond.pool import Pool
from .production import *


def register():
    Pool.register(
        Production,
        module='production_output_location', type_='model')