# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import timedelta
from decimal import Decimal
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, If, Or
from trytond.transaction import Transaction

__all__ = ['Production']

__metaclass__ = PoolMeta


class Production:
    __name__ = 'production'
    output_location = fields.Many2One('stock.location', 'Output Location',
                                      domain=[('type', '=', 'storage'),
                                              ('parent', 'child_of', [Eval('warehouse', -1)], 'parent')],
                                      states={'readonly': (~Eval('state').in_(['request', 'draft'])
                                                           | Eval('inputs', True) | Eval('outputs', True))},
                                      depends=['warehouse', 'state'])

    @classmethod
    def default_output_location(cls):
        pool = Pool()
        Location = pool.get('stock.location')

        warehouse_id = cls.default_warehouse()
        if warehouse_id:
            locations = Location.search([('type', '=', 'storage'),
                                         ('parent', 'child_of', [warehouse_id], 'parent')])
            if len(locations) == 1:
                return locations[0].id

    @fields.depends('output_location')
    def on_change_bom(self):
        return super(Production, self).on_change_bom()

    @fields.depends('output_location')
    def on_change_uom(self):
        return super(Production, self).on_change_uom()

    @fields.depends('output_location')
    def on_change_quantity(self):
        return super(Production, self).on_change_quantity()

    @fields.depends('output_location')
    def on_change_product(self):
        return super(Production, self).on_change_product()

    def explode_bom(self):
        changes = super(Production, self).explode_bom()

        if not changes:
            return {}
        if not getattr(self, 'output_location', None):
            return changes
        storage_location = self.warehouse.storage_location if self.warehouse else None
        out_location = getattr(self, 'output_location', None)
        if storage_location and storage_location.id == out_location.id:
            return changes
        if not 'outputs' in changes:
            return changes

        outputs = changes['outputs']
        if not 'add' in outputs:
            return changes
        for add in outputs['add']:
            item = add[1]
            item['to_location'] = out_location.id
            item['to_location.rec_name'] = out_location.rec_name

        return changes